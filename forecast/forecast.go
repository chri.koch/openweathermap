package forecast

import (
	"encoding/json"
	//"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/chri.koch/openweathermap/config"
)

type WeatherInfoResponse struct {
	List []WeatherInfo `json:"list"`
}

type WeatherInfoTemp struct {
	Min float64 `json:"min"`
	Max float64 `json:"max"`
}

type WeatherInfo struct {
	Dt      int64
	Temp    WeatherInfoTemp `json:"temp"`
	Weather []struct {
		Id int `json:"id"`
	} `json:"weather"`
}

type Forecast struct {
	Config config.Config

	currentForecast WeatherInfoResponse
}

func (f *Forecast) TodayWeather() *WeatherInfo {
	for _, e := range f.currentForecast.List {
		t := time.Unix(e.Dt, 0)
		if t.Local().Year() == time.Now().Local().Year() &&
			t.Local().YearDay() == time.Now().Local().YearDay() {

			return &e
		}
	}

	return nil
}

func (w *WeatherInfo) String() string {
	var s string

	s = fmt.Sprintf("%v, TempMin: %v, TempMax: %v",
		time.Unix(w.Dt, 0).Format(time.RFC850),
		w.Temp.Min,
		w.Temp.Max)

	s += " Weather conditions:"

	for _, v := range w.Weather {
		s += fmt.Sprintf(" %v", v.Id)
	}

	return s
}

func (w *WeatherInfo) Rain() bool {
	for _, v := range w.Weather {
		if v.Id >= 500 && v.Id < 600 {
			return true
		}
	}

	return false
}

func (w *WeatherInfo) Sun() bool {
	for _, v := range w.Weather {
		if v.Id == 800 {
			return true
		}
	}

	return false
}

func (w *WeatherInfo) Snow() bool {
	for _, v := range w.Weather {
		if v.Id >= 600 && v.Id < 700 {
			return true
		}
	}

	return false
}

func (f *Forecast) Update() error {
	req, err := http.NewRequest("GET", f.Config.ForecastBaseUrl(), nil)
	if err != nil {
		log.Println(err)
		return err
	}

	query := req.URL.Query()

	query.Add("id", strconv.Itoa(f.Config.CityId))
	query.Add("APPID", f.Config.APIkey)
	query.Add("cnt", "1")
	query.Add("units", "metric")

	req.URL.RawQuery = query.Encode()

	var client http.Client

	log.Printf("Fetching current weather from %v\n", req.URL)

	resp, err := client.Do(req)
	if err != nil {
		log.Println(err)
		return err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return err
	}

	log.Println("Weather result: " + string(body))

	err = json.Unmarshal(body, &f.currentForecast)
	if err != nil {
		log.Println(err)
		return err
	}

	/*	for _, e := range f.currentForecast.List {
			t := time.Unix(e.Dt, 0)
			log.Println(t.Local().String())
			log.Println(e.Temp.Min)
			log.Println(e.Temp.Max)
			log.Println(e.Weather[0].Id)
		}
	*/
	//log.Println(info.Cod)

	return nil
}
