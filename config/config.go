//holds the configuration
package config

type Config struct {
	APIkey string
	CityId int
}

func (c *Config) ForecastBaseUrl() string {
	return "http://api.openweathermap.org/data/2.5/forecast/daily"
}
